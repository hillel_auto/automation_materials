import sys
import json
import yaml
import pprint
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('path', help="Path to json file to print")
parser.add_argument('-v', '--verbose', dest="verbose", action='store_true', help="Verbose") # -v  # True /False
parser.add_argument('-t','--type',
                    const='all',
                    nargs='?',
                    choices=['json', 'yaml', 'all'],
                    help='list servers, storage, or both (default: %(default)s)')

mgrp = parser.add_mutually_exclusive_group()
mgrp.add_argument('--xml', action='store_true', help="Path to json file to print")
mgrp.add_argument('--yaml', action='store_true', help="Path to json file to print")
mgrp.add_argument('--json', action='store_true', help="Path to json file to print")

args = parser.parse_args()

if args.type == 'json':
    with open(args.path, encoding='utf-8') as f:
        data = json.load(f)
print(args.type)
# if args.type == 'yaml':
with open(args.path, encoding='utf8') as fp:
    data = yaml.load(fp, Loader=yaml.SafeLoader)

sys.argv # list with arguments
# if len(sys.argv) < 2:
#     raise RuntimeError("Param path for json file not set!")
if args.verbose:
    pprint.pprint(data["name"])
    pprint.pprint(data.get("net_params"))
